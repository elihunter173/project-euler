"""
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
"""

# Euclidean algorithm for GCF
# (https://www.khanacademy.org/computing/computer-science/cryptography/modarithmetic/a/the-euclidean-algorithm)
def gcf(a, b):
    if a == 0:
        return b
    elif b == 0:
        return a
    else:
        return gcf(b, a % b)


def lcm(a, b):
    return (a * b) / gcf(a, b)  # LCM(n1,n2) = (n1 * n2) / GCF(n1,n2)


def compute(first, last):
    ans = 1
    for n in range(first, last):
        ans = lcm(ans, n)  # LCM is commutative, associative, and idempotent

    return int(ans)  # makes it pretty because it's always an int


print(compute(1, 20))
