#!/bin/bash

dest_file=../ANSWERS.txt

if [ -f $dest_file ]; then
    rm $dest_file
fi

echo "Project Euler Answers" >> $dest_file
echo "Computed by Eli W. Hunter" >> $dest_file
echo "" >> $dest_file

for file in *.py; do
    problem_number=${file#p}
    problem_number=${problem_number%.py}

    echo "Problem $problem_number: $(python3 $file)" >> $dest_file
done
