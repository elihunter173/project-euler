"""
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?

Imported from elihunter173/PrimeFinder
"""

import math


def is_prime(test_number, found_primes):
    """Determine if n is prime given a list of all primes less than n.

    Args:
        test_number is the number being tested to see if it's prime
        found_primes is the list of all numbers which are less than test_number
        and prime
    """
    for current_prime in found_primes:
        # This is the dropout condiiton.
        if current_prime > math.sqrt(test_number):
            return True
        if test_number % current_prime == 0:  # tests for factors
            return False

    # if found_primes is empty, just return true.
    return True


def find_primes(primes_to_find):
    """Find primes up to primes_to_find.

    This method finds primes by starting with an empty list of primes and then
    testing if the testing-number has any factors inside of the list of found
    primes.

    Args:
        primes_to_find is the number of primes to find.
    """
    found_primes = []
    test_number = 2  # the first prime

    while len(found_primes) < primes_to_find:
        if is_prime(test_number, found_primes):
            found_primes.append(test_number)
        test_number += 1
    return found_primes


primes = find_primes(10001)
print(primes[-1])
