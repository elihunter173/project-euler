"""
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
"""

import math


def largestPrimeFactor(n):
    while True:
        if n == smallestPrime(n): #  if the number is prime itself, largest prime factor found
            return n
        else:
            n //= smallestPrime(n)  # if number still composite, floor divide away its smallest prime


def smallestPrime(n):
    for i in range(2, math.ceil(math.sqrt(n))):  # loop thru all possible numbers which could be a factor of n ( # >= 2 and # <= sqrt(n))
        if n % i == 0:
            return i
    return n


print(largestPrimeFactor(600851475143))
