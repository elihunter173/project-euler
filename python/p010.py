"""
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.
"""


from itertools import repeat


def prime_sieve(max_number):
    """Find all primes below limit using Sieve of Eratosthenes.

    Args:
        max_number (int): The upper bound that the sieve checks from.
        Exclusive.

    Returns:
        iterator of iterator (int, bool): A list of all numbers
        greater than 2 below max_number. The bool is True if prime.
        False otherwise
    """
    # Subtract 3 because 2 is the first number of the list (since the
    # Sieve of Eratosthenes starts with 2) and lists start at 1.
    last_index = max_number - 2
    assert last_index >= 0
    is_prime_list = list(repeat(True, last_index))

    for current_number in range(2, max_number):
        current_index = current_number - 2

        if is_prime_list[current_index]:
            # If the number is a prime, remove its multiples
            for i in range(current_index + current_number, last_index, current_number):
                is_prime_list[i] = False

    return list(zip(range(2, limit), is_prime_list))


if __name__ == '__main__':
    # If this is being run independently
    # This is the "main method"
    limit = 2000000

    conditioned_primes = prime_sieve(limit)
    prime_sum = sum(number for number, condition in conditioned_primes if condition)

    print(prime_sum)
