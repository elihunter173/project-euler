"""
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
"""


def isPalindrome(n):
    return str(n) == str(n)[::-1]


def largestPalindromeProduct(number_of_digits):
    largestPalindrome = max(x * y
                            for x in range(1, 10 ** number_of_digits)
                            for y in range(1, 10 ** number_of_digits)
                            if isPalindrome(x * y))
    return largestPalindrome


print(largestPalindromeProduct(3))
